﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Bootproject.Core.Services
{
    public class PageService : IPageService
    {
        private const string PAGES = "Pages";
        private readonly string _folder;
        private readonly List<Page> _cache = new List<Page>();
        private readonly IHttpContextAccessor _contextAccessor;

        public PageService(IHostingEnvironment env, IHttpContextAccessor contextAccessor)
        {
            _folder = Path.Combine(env.WebRootPath, PAGES);
            _contextAccessor = contextAccessor;
            _cache.Clear();
            //Initialize();
        }


        public virtual Task<List<Page>> GetPages()
        {
            var pages = new List<Page>
            {
                new Page
                {
                    Title="Bootproject",
                    Published= true,
                    Slug="/",
                    SortOrder=0,
                    Contents = new List<Content>{
                        new Content { Title="Bootproject", Html="<h2>Bootproject</h2><p>This is the first content.</p>" },
                         new Content { Title="Second", Html="<h2>Another content</h2><p>This is the second content.</p>" },
                         new Content { Title="Third", Html="<h2>Another content</h2><p>This is the third content.</p>" }
                    }
                },
                 new Page
                {
                    Title="Page two",
                    Published= true,
                    Slug="pagetwo",
                    SortOrder=1,
                    Contents = new List<Content>{
                        new Content { Title="Content one", Html="<h2>Content one</h2><p>This is the first content.</p>" }
                    }
                },
                 new Page
                 {
                    Title="Blog",
                    Published= true,
                    SortOrder=2,
                    Slug="/blog",
                    Contents = new List<Content>()
                 }
            };
            return Task.FromResult(pages);

        }

        public virtual Task AddContent(Page page)
        {
            page.Contents.Add(new Content { Title="Add title" });
            return Task.FromResult(page);
        }

        public virtual Task DeleteContent(Content content)
        {
            throw new NotImplementedException();
        }

        public virtual Task DeletePage(Page page)
        {
            throw new NotImplementedException();
        }


        public virtual async Task SavePageAsync(Page page)
        {
            string filePath = Path.Combine(_folder, page.Id + ".xml");


            XDocument doc = new XDocument(
                            new XElement("post",
                                new XElement("title", post.Title),
                                new XElement("slug", post.Slug),
                                new XElement("pubDate", FormatDateTime(post.PubDate)),
                                new XElement("lastModified", FormatDateTime(post.LastModified)),
                                new XElement("excerpt", post.Excerpt),
                                new XElement("content", post.Content),
                                new XElement("ispublished", post.IsPublished),
                                new XElement("categories", string.Empty),
                                new XElement("comments", string.Empty)
                            ));

            XElement categories = doc.XPathSelectElement("post/categories");
            foreach (string category in post.Categories)
            {
                categories.Add(new XElement("category", category));
            }

            XElement comments = doc.XPathSelectElement("post/comments");
            foreach (Comment comment in post.Comments)
            {
                comments.Add(
                    new XElement("comment",
                        new XElement("author", comment.Author),
                        new XElement("email", comment.Email),
                        new XElement("date", FormatDateTime(comment.PubDate)),
                        new XElement("content", comment.Content),
                        new XAttribute("isAdmin", comment.IsAdmin),
                        new XAttribute("id", comment.ID)
                    ));
            }

            using (var fs = new FileStream(filePath, FileMode.Create, FileAccess.ReadWrite))
            {
                await doc.SaveAsync(fs, SaveOptions.None, CancellationToken.None).ConfigureAwait(false);
            }

            if (!_cache.Contains(page))
            {
                _cache.Add(page);
            }

            throw new NotImplementedException();
        }
    }
}
