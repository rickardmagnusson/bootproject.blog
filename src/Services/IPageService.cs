﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bootproject.Core.Services
{
    public interface IPageService
    {
        Task<List<Page>> GetPages();

        Task SavePageAsync(Page page);

        Task DeletePage(Page page);

        Task AddContent(Page page);

        Task DeleteContent(Content content);

        
    }

    public abstract class IMemoryPageServiceBase
    {
        protected IHttpContextAccessor ContextAccessor { get; }
        protected List<Page> Cache { get; set; }

        public IMemoryPageServiceBase(IHttpContextAccessor contextAccessor)
        {
            ContextAccessor = contextAccessor;
        }

        public virtual Task<IEnumerable<Page>> GetPages()
        {
            var posts = Cache
                .Where(p => p.Published);

            return Task.FromResult(posts);
        }
    }


    public class Page
    {
        public string Id { get; set; }
        public string Slug { get; set; }
        public string Title { get; set; }
        public int SortOrder { get; set; }
        public bool Published { get; set; }
        public List<Content> Contents { get; set; }
        public bool Current { get; set; } = false;
    }

    public class Content
    {
        public string Title { get; set; }

        public string Html { get; set; }
    }
}
