﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;
using Bootproject.Core.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Miniblog.Core.Models;
using Miniblog.Core.Services;
using WebEssentials.AspNetCore.Pwa;

namespace Miniblog.Core.Controllers
{
    public class PageController : Controller
    {
        private readonly IPageService _pages;
        private readonly List<Page> Pages;

        public PageController(IPageService pageService)
        {
            _pages = pageService;
            Pages = _pages.GetPages().Result;
            ViewData["Pages"] = Pages;
        }

        [Route("/{slug?}")]
        public async Task<IActionResult> Index([FromRoute]string slug = "")
        {
          
            Pages.ForEach(p => p.Current = false);
           

            if (!string.IsNullOrEmpty(slug))
            {
                var page = Pages.Where(s => s.Slug == slug).First();
                page.Current = true;
                ViewData["Pages"] = Pages;
                return View("~/Views/Page/Index.cshtml", page);
            }
            else
            {
                var page = Pages.Where(s => s.Slug == "/").First();
                page.Current = true;
                ViewData["Pages"] = Pages;
                return View("~/Views/Page/Index.cshtml", page);
            }
        }

        [Route("/edit/{slug?}")]
        public async Task<IActionResult> Edit([FromRoute]string slug)
        {
            Page page;
            ViewData["Pages"] = Pages;
            if (string.IsNullOrEmpty(slug))
                page = Pages.Where(s => s.Slug == "/").First();
            else
                page = Pages.Where(p=> p.Slug.Equals(slug)).First();

            return View(page);
        }

        [Route("/addcontent/{slug?}")]
        public async Task<IActionResult> AddContent(string slug)
        {
            Page page;
           
            if (string.IsNullOrEmpty(slug))
                page = Pages.Where(s => s.Slug == "/").First();
            else
                page = Pages.Where(p => p.Slug.Equals(slug)).First();

            if (page != null)
            {
                page.Contents.Add(new Content { Title = "Add title" });
                ViewData["Pages"] = Pages;
                return View("~/Views/Page/Edit.cshtml", page);
            }

            return NotFound();
        }

        [Route("/edit/{slug?}")]
        [HttpPost]
        public async Task<IActionResult> UpdatePost(Page page)
        {

            foreach(var content in page.Contents)
            {

            }

            return NotFound();
        }

    }
}
