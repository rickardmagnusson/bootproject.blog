﻿using Bootproject.Core.Services;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Miniblog.Core
{
    public static class HtmlHelpers
    {

        public static List<Page> GetPages()
        {
            var pages = new List<Page>
            {


                new Page
                {
                    Title="Bootproject",
                    Published= true,
                    Slug="/",
                    SortOrder=0,
                    Contents = new List<Content>{
                        new Content { Title="Bootproject", Html="<h1>Bootproject</h1><p>This is the first content.</p>" }
                    }
                },
                 new Page
                {
                    Title="Page two",
                    Published= true,
                    Slug="pagetwo",
                    SortOrder=0,
                    Contents = new List<Content>{
                        new Content { Title="Content one", Html="<h1>Content one</h1><p>This is the first content.</p>" }
                    }
                },
                 new Page
                 {
                    Title="Blog",
                    Published= true,
                    SortOrder=2,
                    Slug="/blog",
                    Contents = new List<Content>()
                 }
            };
            return pages;
        }
    }
}
